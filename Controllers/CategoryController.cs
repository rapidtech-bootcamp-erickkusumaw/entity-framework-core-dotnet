using System;
using EfDotnet.DataContext;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace EfDotnet.Controllers;

// [Route("[controller]")]
public class CategoryController : Controller
{
    private readonly ApplicationDbContext _context;
    public CategoryController(ApplicationDbContext context)
    {
        _context = context;
    }

    [HttpGet]
    public IActionResult Index()
    {
        IEnumerable<CategoryEntity> categories = _context.CategoryEntities.ToList();
        return View(categories);
    }

    [HttpGet("[action]")]
    public IActionResult Details(int? id)
    {
        CategoryEntity category = _context.CategoryEntities.Find(id);
        return View(category);
    }

    [HttpGet("[action]")]
    public IActionResult Add()
    {
        return View();
    }

    [HttpPost]
    public IActionResult Save([Bind("CategoryCode, CategoryName, Description")] CategoryEntity request)
    {
        // add ke entity
        _context.CategoryEntities.Add(request);
        // commit to database
        _context.SaveChanges();

        return Redirect("Index");
    }

    [HttpGet("[action]")]
    public IActionResult Edit(int? id)
    {
        var entity = _context.CategoryEntities.Find(id);
        return View(entity);
    }

    [HttpGet("[action]")]
    public IActionResult Delete(int? id)
    {
        var entity = _context.CategoryEntities.Find(id);
        if (entity == null)
        {
            return Redirect("Index");
        }
        // remove from entity
        _context.CategoryEntities.Remove(entity);
        // commit to database
        _context.SaveChanges();
        return Redirect("Index");
    }

    [HttpPost]
    public IActionResult Update([Bind("Id,CategoryCode, CategoryName, Description")] CategoryEntity request)
    {
        // update entity
        _context.CategoryEntities.Update(request);
        // commit to database
        _context.SaveChanges();
        return Redirect("Index");
    }
}