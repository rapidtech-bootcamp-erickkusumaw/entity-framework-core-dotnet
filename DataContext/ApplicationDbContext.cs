using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Policy;
using Microsoft.Extensions.Options;

namespace EfDotnet.DataContext
{
    public class ApplicationDbContext : DbContext
    {

        // injection
        // public ApplicationDbContext(DbContextOptionsBuilder optionsBuilder)
        // {
        //     optionsBuilder.UseMySQL("server=localhost;database=efdb;user=root;password=root");
        // }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }


        public DbSet<CategoryEntity> CategoryEntities => Set<CategoryEntity>();
        // public DbSet<AssetEntity> AssetEntities => Set<AssetEntity>();



        // FIX error
        // Unable to resolve service for type 'Microsoft.EntityFrameworkCore.Storage.TypeMappingSourceDependencies'
        // while attempting to activate 'MySql.EntityFrameworkCore.Storage.Internal.MySQLTypeMappingSource'.
        public class MysqlEntityFrameworkDesignTimeServices : IDesignTimeServices
        {
            public void ConfigureDesignTimeServices(IServiceCollection serviceCollection)
            {
                serviceCollection.AddEntityFrameworkMySql();
                new EntityFrameworkRelationalDesignServicesBuilder(serviceCollection)
                    .TryAddCoreServices();
            }
        }
        // End Fix

    }

}